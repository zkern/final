﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Vuforia
{
    public class Question : MonoBehaviour
    {

        public Transform TextTargetName;
        public Transform TextDescription;
        public Transform PanelDescription;
        public Transform ButtonText, ButtonText1, ButtonText2;
        public Button Button1;
        public Button Button2;
        public Button Button3;
        public int a = 7;
        public int b = 5;
        public string c = "china";


        void Start()
        {
            
        }


        void Update()
        {
           
            StateManager sm = TrackerManager.Instance.GetStateManager();
            IEnumerable<TrackableBehaviour> x = sm.GetActiveTrackableBehaviours();

            foreach (TrackableBehaviour tb in x)
            {
                string name = tb.TrackableName;
                ImageTarget im = tb.Trackable as ImageTarget;
                Vector2 size = im.GetSize();


                Debug.Log("Active image target:" + name + " -size:" + size.x); //", " + size.y);

                TextTargetName.GetComponent<Text>().text = name;
                TextDescription.gameObject.SetActive(true);
                PanelDescription.gameObject.SetActive(true);
                Button1.gameObject.SetActive(true);
                Button2.gameObject.SetActive(true);
                Button3.gameObject.SetActive(true);
                

                if (name == "Question1")
                {
                    ButtonText.GetComponent<Text>().text = "6";
                    ButtonText1.GetComponent<Text>().text = "7";
                    ButtonText2.GetComponent<Text>().text = "8";

                    TextDescription.GetComponent<Text>().text = "How Many Harry Potter books are there? ";
                    Button Button1 = GetComponent<Button>();
                    Button Button2 = GetComponent<Button>();
                    Button Button3 = GetComponent<Button>();
                    Button a = Button1.GetComponent<Button>();
                    Button b = Button2.GetComponent<Button>();
                    Button c = Button3.GetComponent<Button>();
                    a.onClick.AddListener(Incorrect);
                    b.onClick.AddListener(Correct);
                    c.onClick.AddListener(Incorrect);



                    //Button1.GetComponentsInChildren<Text>()[0].text = "New Super Cool Button Text";






                }
                if (name == "Question2")
                {
                    TextDescription.GetComponent<Text>().text = "How many Sharknado movies have been released? ";
                    ButtonText.GetComponent<Text>().text = "5";
                    ButtonText1.GetComponent<Text>().text = "6";
                    ButtonText2.GetComponent<Text>().text = "7";

                }
                if (name == "Question3")
                {
                    TextDescription.GetComponent<Text>().text = "In which country do wild giant pandas live?";
                    ButtonText.GetComponent<Text>().text = "India";
                    ButtonText1.GetComponent<Text>().text = "China";
                    ButtonText2.GetComponent<Text>().text = "Afghanistan";


                }




            }

       }
        void Incorrect()
        {
            //GetComponent<Button>().GetComponent<Image>().color = Color.green;
        }
        void Correct()
        {
            //Debug.Log();
        }
    }
}